==========================
README
==========================
Storm環境を構築するためのプレイブック集。 storm.ymlをエントリポイントとして、各プレイブックを実行する。

storm.ymlの中では、以下のプレイブックを順に実行する。

* storm_cluster.yml - Stormクラスタ全体に関係する設定を行う
* storm_zookeeper.yml - Zookeeperアンサンブルを構成するサーバ上で必要な設定を行う
* storm_nimbus.yml - Nimbusを動作させるサーバで必要な設定を行う
* storm_supervisor.yml - Supervisorを動作させるサーバで必要な設定を行う
* storm_client.yml - Stormのクライアントとして利用するサーバの設定を行う

各プレイブックの実行内容は以下のとおり。

storm_cluster.yml::

   roles:
     - common
     - users
     - screen
     - ntp
     - prompt
     - epel
     - jdk

storm_zookeeper.yml::

   roles:
     - zookeeper

storm_nimbus.yml::

   roles:
     - { role: zeromq, when: storm_version < "0.9.1" }
     - { role: jzmq, when: storm_version < "0.9.1" }
     - storm

storm_supervisor.yml::

   roles:
     - { role: zeromq, when: storm_version < "0.9.1" }
     - { role: jzmq, when: storm_version < "0.9.1" }
     - storm

storm_client.yml::

   roles:
     - { role: zeromq, when: storm_version < "0.9.1" }
     - { role: jzmq, when: storm_version < "0.9.1" }
     - storm

