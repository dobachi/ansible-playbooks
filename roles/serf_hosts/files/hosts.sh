#!/bin/sh
 
hosts='/etc/hosts'
event=${SERF_EVENT}
echo ${event} > /tmp/test
 
read member
name=`echo ${member} | cut -d ' ' -f 1`
addr=`echo ${member} | cut -d ' ' -f 2`
role=`echo ${member} | cut -d ' ' -f 3`
 
if [ ! -w ${hosts} ]; then
  echo "Cannot write file: ${hosts}" 1>&2
  exit 1
fi
 
if [ ${event} = 'member-join' ]; then
  if ! grep ${name} ${hosts} > /dev/null; then
    echo -e "${addr} ${name}" >> ${hosts}
  fi
elif [ ${event} = 'member-leave' ] || [ ${event} = 'member-failed' ]; then
  sed -i "/${name}/d" ${hosts}
fi
