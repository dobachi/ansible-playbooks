######################
概要
######################
このロールは、spark-0.8.0のソースコードをダウンロードするためのロールである。
ダウンロード先は、 /usr/src/spark以下である。

なお、コンパイルは現在のところ実施しない。
各ユーザのホームディレクトリに /usr/src/spark/spark-0.8.0-incubating をコピーしたのち、
各自コンパイルすること。

コマンド例::

 $ mkdir -p ~/Sources/spark
 $ cp -r /usr/src/spark/spark-0.8.0-incubating ~/Sources/spark
 $ cd ~/Sources/spark/spark-0.8.0-incubating 
 $ sbt/sbt assembly

コンパイルした後にREPLを起動すると、Sparkをオンメモリで単独起動できる。

コマンド例::

 $ ./spark-shell
 spark>

.. vim: ft=rst tw=0
