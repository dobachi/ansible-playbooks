Name:           global
Version:        6.2.9
Release:        3.1%{?dist}
Summary:        Source code tag system
Group:          Development/Tools
License:        GPLv2+ and BSD
URL:            http://www.gnu.org/software/global
Source:         ftp://ftp.gnu.org/pub/gnu/global/global-%{version}.tar.gz
BuildRequires:  ncurses-devel, ctags

%description
GNU GLOBAL is a source code tag system that works the same way across
diverse environments. It supports C, C++, Yacc, Java, PHP and
assembler source code.

%package -n emacs-global
Summary: GNU GLOBAL support for Emacs
Group: Applications/Editors
BuildArch: noarch
BuildRequires: emacs
Requires: %{name} = %{version}-%{release}, emacs(bin) >= %{_emacs_version}

%description -n emacs-global
%{summary}.

%package -n emacs-global-el
Summary: Elisp source files for GNU GLOBAL support for Emacs
Group: Applications/Editors
BuildArch: noarch
Requires: emacs-global = %{version}-%{release}

%description -n emacs-global-el
%{summary}.


%prep
%setup -q

%build
%configure --with-posix-sort=/bin/sort --with-exuberant-ctags=/usr/bin/ctags
make %{?_smp_mflags}
%{_emacs_bytecompile} gtags.el

%install
make install DESTDIR=%{buildroot}

# Remove empty useless directory
rm -f %{buildroot}%{_infodir}/dir
# Remove example plug-in
#rm -rf %{buildroot}%{_libdir}/gtags

rm %{buildroot}/%{_datadir}/gtags/{gtags.el,gtags.conf}
rm %{buildroot}/%{_datadir}/gtags/{AUTHORS,BOKIN_MODEL,BOKIN_MODEL_FAQ,COPYING,ChangeLog,DONORS,FAQ,INSTALL,LICENSE,NEWS,README,THANKS}

mkdir -p %{buildroot}%{_sysconfdir}
install gtags.conf -t %{buildroot}%{_sysconfdir}

mkdir -p %{buildroot}%{_emacs_sitelispdir}
install gtags.el gtags.elc -t %{buildroot}%{_emacs_sitelispdir}

## Remove executable flag
chmod -x %{buildroot}/%{_sysconfdir}/gtags.conf

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir 2>/dev/null || :
ln -sf %{_libdir}/gtags /usr/lib/gtags 2>/dev/null 

%preun
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/%{name}.info \
    %{_infodir}/dir 2>/dev/null || :
fi

%files
%doc README THANKS LICENSE AUTHORS COPYING FAQ NEWS BOKIN_MODEL
%doc BOKIN_MODEL_FAQ DONORS ChangeLog
%config(noreplace) %{_sysconfdir}/gtags.conf
%{_bindir}/*
%{_infodir}/global.info*
%{_mandir}/man*/*
%{_datadir}/gtags
%{_libdir}/gtags/*

%files -n emacs-global
%{_emacs_sitelispdir}/*.elc

%files -n emacs-global-el
%{_emacs_sitelispdir}/*.el

%changelog
* Thu Dec 19 2013 Kazumi Takeuchi <iqu1979@gmail.com> - 6.2.9-3.1
- Include exuberant-ctags

* Tue Oct 08 2013 Pavel Zhukov <landgraf@fedoraproject.org> - 6.2.9-3
- Remove deprecated defattr
- Cosmetic changes after review

* Mon Oct 07 2013 Pavel Zhukov <landgraf@fedoraproject.org> - 6.2.9-1
- Unorphan package
- New release 6.2.9

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.9.3-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.9.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.9.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Dec  8 2010 Karel Klic <kklic@redhat.com> - 5.9.3-2
- Added --with-posix-sort=/bin/sort to %%configure to speed up
  indexing.

* Wed Dec  8 2010 Karel Klic <kklic@redhat.com> - 5.9.3-1
- Newest upstream release.

* Wed Dec  8 2010 Karel Klic <kklic@redhat.com> - 5.7.5-2
- Build gtags.elc and package it in the new emacs-global package.
- Package gtags.el in the new emacs-global-el package.
- Removed docs from /usr/share/gtags.
- Added more docs to %%doc.
- Install default gtags.conf into /etc.
- Removed deprecated %%clean section.
- Removed unused BuildRoot tag.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.7.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jun 16 2009 Gerard Milmeister <gemi@bluewin.ch> - 5.7.5-1
- new release 5.7.5

* Fri Nov 21 2008 Gerard Milmeister <gemi@bluewin.ch> - 5.7.3-1
- new release 5.7.3

* Sun Oct 19 2008 Gerard Milmeister <gemi@bluewin.ch> - 5.7.2-1
- new release 5.7.2

* Sat Aug  2 2008 Gerard Milmeister <gemi@bluewin.ch> - 5.7.1-1
- new release 5.7.1

* Mon Jul 21 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 5.4-3
- fix license tag

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 5.4-2
- Autorebuild for GCC 4.3

* Sat Feb 24 2007 Gerard Milmeister <gemi@bluewin.ch> - 5.4-1
- new version 5.4

* Sat Dec  2 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.3-1
- new version 5.3

* Wed Aug 30 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.2-1
- new version 5.2

* Mon Aug 28 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.0-2
- Rebuild for FE6

* Sun Apr 30 2006 Gerard Milmeister <gemi@bluewin.ch> - 5.0-1
- new version 5.0

* Fri Feb 17 2006 Gerard Milmeister <gemi@bluewin.ch> - 4.8.7-3
- Rebuild for Fedora Extras 5

* Wed Oct  5 2005 Gerard Milmeister <gemi@bluewin.ch> - 4.8.7-2
- Remove dir in /usr/share/info

* Sat Oct  1 2005 Gerard Milmeister <gemi@bluewin.ch> - 4.8.7-1
- New Version 4.8.7

* Tue Jul  5 2005 Gerard Milmeister <gemi@bluewin.ch> - 4.8.6-1
- New Version 4.8.6

* Wed May 25 2005 Jeremy Katz <katzj@redhat.com> - 4.8.4-4
- fix build with gcc4 (#156212)

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 4.8.4-3
- rebuild on all arches

* Thu Apr 07 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Mon Mar  7 2005 Gerard Milmeister <gemi@bluewin.ch> - 4.8.4-1
- New Version 4.8.4

* Mon Dec 27 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:4.8.2-0.fdr.1
- New Version 4.8.2

* Sat Oct 23 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:4.8.1-0.fdr.1
- New Version 4.8.1

* Sat Jul 17 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:4.7.2-0.fdr.1
- New Version 4.7.2

* Fri Mar 19 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:4.7-0.fdr.1
- New Version 4.7

* Thu Nov 27 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:4.6.1-0.fdr.1
- First Fedora release

